import { Render } from './render';
import { movieSearchServices, movieServices } from '../../../services';
import { MovieCategories } from '../../../config';
import {
    transformFromTotalToMovie,
    FavoritesLocalStorage,
} from '../../../helpers';
import { IMoviesStore } from '../../../types';

export class Direct {
    state = {
        loading: false,
        category: MovieCategories.LOAD_MORE,
        page: 1,
        pages: 1,
        query: '',
    };
    localStorage: InstanceType<typeof FavoritesLocalStorage>;
    render: InstanceType<typeof Render>;
    moviesStore: IMoviesStore;
    constructor(
        localStorage: InstanceType<typeof FavoritesLocalStorage>,
        render: InstanceType<typeof Render>,
        moviesStore: IMoviesStore
    ) {
        this.localStorage = localStorage;
        this.render = render;
        this.moviesStore = moviesStore;
    }

    async newCategories(category: MovieCategories): Promise<void> {
        if (this.state.loading || category === this.state.category) {
            return;
        }
        this.setPage(false);
        this.render.setLoading();
        try {
            const data = await movieServices.load(category);
            this.state.pages = data.total_pages;
            this.state.category = category;
            const movies = transformFromTotalToMovie(
                data,
                this.localStorage.getItem()
            );
            this.moviesStore.reset();
            this.render.resetMovies();
            this.moviesStore.add(movies);
            this.render.addMovies(movies);
        } catch (e) {
            console.error(e?.message || 'unknown error');
        }
        this.state.loading = false;
        this.render.removeLoading();
    }

    async nextPage(): Promise<void> {
        if (this.state.loading || !this.state.category.length) {
            return;
        }
        this.setPage(true);
        this.render.setLoading();
        if (this.state.page > this.state.pages) {
            return;
        }
        try {
            const data = await movieServices.load(
                this.state.category,
                this.state.page
            );
            const movies = transformFromTotalToMovie(
                data,
                this.localStorage.getItem()
            );
            this.render.addMovies(movies);
            this.moviesStore.add(movies);
        } catch (e) {
            console.error(e?.message || 'unknown error');
        }
        this.state.loading = false;
        this.render.removeLoading();
    }

    async search(query: string, nextPage = false): Promise<void> {
        if (!query.length || this.state.loading) {
            return;
        }
        this.setPage(nextPage);
        if (this.state.page > this.state.pages) {
            return;
        }
        this.render.setLoading();
        this.state.loading = true;
        this.state.category = MovieCategories.SUBMIT;
        try {
            const data = await movieSearchServices.load(query, this.state.page);
            const movies = transformFromTotalToMovie(
                data,
                this.localStorage.getItem()
            );

            this.state.query = query;
            if (!nextPage) {
                this.moviesStore.reset();
                this.render.resetMovies();
                this.state.pages = data.total_pages;
            }
            this.moviesStore.add(movies);
            this.render.addMovies(movies);
        } catch (e) {
            console.error(e?.message || 'unknown error');
        }
        this.state.loading = false;
        this.render.removeLoading();
    }

    nextPageDirect(): void {
        if (this.state.category === MovieCategories.SUBMIT) {
            this.search(this.state.query, true);
        } else {
            this.nextPage();
        }
    }

    setPage(nextPage: boolean): void {
        this.state = {
            ...this.state,
            page: nextPage ? ++this.state.page : 1,
            pages: nextPage ? this.state.pages : 1,
        };
    }
}
