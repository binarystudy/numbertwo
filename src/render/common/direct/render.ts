import { loader, movieCardCreate, setRandomMovie } from '../../components';
import { MovieDataset, MovieIdElements } from '../../../config';
import { IMovie } from '../../../types';

export class Render {
    filmContainer = document.getElementById(MovieIdElements.FILM_CONTAINER);
    favoriteContainer = document.getElementById(MovieIdElements.FAVORITE_MOVIES);
    loader = loader();
    favoriteLoader = loader();

    setLoading(): void {
        if (this.filmContainer) {
            this.filmContainer.append(this.loader);
        }
    }

    removeLoading(): void {
        this.loader.remove();
    }

    addMovies(movies: IMovie[]): void {
        setRandomMovie(movies);
        movies.forEach((movie) => {
            this.filmContainer?.append(movieCardCreate(movie, 'movie'));
        });
    }

    replaceMovieCard(movie: IMovie): void {
        if (!this.filmContainer) {
            return;
        }
        const cards = Array.from(this.filmContainer.children) as HTMLElement[];
        const replaceCard = cards.filter((card) => {
            const idfromElement = card.dataset[MovieDataset.MOVIE_ID];
            if (!idfromElement || !/^\d+/.test(idfromElement)) {
                return false;
            }
            return Number(idfromElement) === movie.id;
        });
        if (!replaceCard.length) {
            return;
        }
        const newCard = movieCardCreate(movie, 'movie');
        this.filmContainer.replaceChild(newCard, replaceCard[0]);
    }

    resetMovies(): void {
        if (!this.filmContainer) {
            return;
        }
        this.filmContainer.innerHTML = '';
    }

    addFavorites(movies: IMovie[]): void {
        if (!this.favoriteContainer) {
            return;
        }
        movies.forEach((movie) => {
            this.favoriteContainer?.append(movieCardCreate(movie, 'favorite'));
        });
    }

    favoriteSetLoading(): void {
        if (!this.favoriteContainer) {
            return;
        }
        this.favoriteContainer.innerHTML = '';
        this.favoriteContainer.append(this.favoriteLoader);
    }

    favoriteRemoveLoading(): void {
        this.favoriteLoader.remove();
    }
}
