import { Render } from './render';
import {
    FavoritesLocalStorage,
    transformFromDetailToMovie,
} from '../../../helpers';
import { movieDetailServices } from '../../../services';
import { IMovie, IMovieDetailResponse, IMoviesStore } from '../../../types';

export class Favorites {
    favorites: IMovie[] = [];
    loading = false;
    localStorage: InstanceType<typeof FavoritesLocalStorage>;
    render: InstanceType<typeof Render>;
    moviesStore: IMoviesStore;
    constructor(
        localStorage: InstanceType<typeof FavoritesLocalStorage>,
        render: InstanceType<typeof Render>,
        moviesStore: IMoviesStore
    ) {
        this.localStorage = localStorage;
        this.render = render;
        this.moviesStore = moviesStore;
    }

    async activate(): Promise<void> {
        const favoritesId = this.localStorage.getItem();
        if (!favoritesId) {
            return;
        }
        this.loading = true;
        this.render.favoriteSetLoading();
        const favoritesWithData: IMovieDetailResponse[] = [];
        try {
            for (const id of favoritesId) {
                const data = await movieDetailServices.load(id);
                if (!data) {
                    continue;
                }
                favoritesWithData.push(data);
            }
            this.favorites = transformFromDetailToMovie(
                favoritesWithData,
                favoritesId
            );
        } catch (e) {
            console.error(e?.message || 'unknown error');
        }
        this.render.favoriteRemoveLoading();
        this.render.addFavorites(this.favorites);
        this.loading = false;
    }

    async changeFavorite(element: HTMLElement, idStr: string): Promise<void> {
        if (!/^\d+$/.test(idStr) || this.loading) {
            return;
        }
        const id = Number(idStr);
        this.changeMoviesState(id);
        const newFavorites = this.favorites.filter(
            (favorite) => favorite.id !== id
        );
        this.loading = true;
        this.render.favoriteSetLoading();
        if (newFavorites.length === this.favorites.length) {
            const favorite = this.moviesStore.getElement(id);
            if (favorite) {
                this.favorites = [...newFavorites, favorite];
            }
        } else {
            this.favorites = [...newFavorites];
        }
        this.localStorage.setItem(
            this.favorites.map((favorite) => favorite.id)
        );
        this.render.favoriteRemoveLoading();
        this.render.addFavorites(this.favorites);
        this.loading = false;
    }

    changeMoviesState(id: number): void {
        const movie = this.moviesStore.getElement(id);
        if (!movie) {
            return;
        }
        const newMovie = {
            ...movie,
            isFavorites: !movie.isFavorites,
        };
        this.moviesStore.change(newMovie);
        this.render.replaceMovieCard(newMovie);
    }
}
