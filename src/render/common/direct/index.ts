import { Render } from './render';
import { Direct } from './direct';
import { Favorites } from './favorites';
import {
    favoritesLocalStorage as localStorage,
    moviesStore,
} from '../../../helpers';

const render = new Render();

export const direct = new Direct(localStorage, render, moviesStore);
export const favoritesDirect = new Favorites(localStorage, render, moviesStore);
