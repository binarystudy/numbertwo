import { handleClick } from './handleClick';

export const addEventListener = (): void => {
    document.body.addEventListener('click', handleClick);
};
