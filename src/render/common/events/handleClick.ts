import {
    navigationButtonsId,
    MovieCategories,
    MovieIdElements,
    MovieDataset,
} from '../../../config';
import { direct, favoritesDirect } from '../direct';

const getSearch = (): string => {
    const text = (document.getElementById(MovieIdElements.SEARCH) as HTMLInputElement)
        ?.value;
    return text ? text : '';
};

const handleFavorite = (element: HTMLElement) => {
    const id = element.dataset[MovieDataset.MOVIE_ID];
    if (!id) {
        return;
    }
    favoritesDirect.changeFavorite(element, id);
};

const handleChangeCategories = (category: MovieCategories): void => {
    switch (category) {
        case MovieCategories.POPULAR:
            direct.newCategories(MovieCategories.POPULAR);
            return;
        case MovieCategories.TOP_RATED:
            direct.newCategories(MovieCategories.TOP_RATED);
            return;
        case MovieCategories.UPCOMING:
            direct.newCategories(MovieCategories.UPCOMING);
            return;
        case MovieCategories.LOAD_MORE:
            direct.nextPageDirect();
            return;
        case MovieCategories.SUBMIT:
            direct.search(getSearch());
            return;
        default:
            return;
    }
};

export const handleClick = (event: MouseEvent): void => {
    const target = event.target as HTMLElement;
    const element = target.closest(`#${MovieIdElements.MOVIE_CARD}`) as HTMLElement;
    if (element) {
        handleFavorite(element);
    }
    if (target.id && navigationButtonsId.includes(target.id)) {
        handleChangeCategories(target.id as MovieCategories);
    }
};
