import { MovieCategories } from '../config';
import { addEventListener, direct, favoritesDirect } from './common';

export const app = (): void => {
    addEventListener();
    direct.newCategories(MovieCategories.POPULAR);
    favoritesDirect.activate();
};
