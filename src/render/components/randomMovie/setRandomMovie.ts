import { MovieIdRendomElements } from '../../../config';
import { IMovie } from '../../../types';
import { randomFromTo } from '../../helpers';

export const setRandomMovie = (data: IMovie[]): void => {
    if (!data.length) {
        return;
    }
    const movie = data[randomFromTo(0, data.length - 1)];
    const name = document.getElementById(MovieIdRendomElements.NAME);
    const description = document.getElementById(
        MovieIdRendomElements.DESCRIPTION
    );
    if (name) {
        name.innerText = movie.title;
    }
    if (description) {
        description.innerText = movie.overview;
    }
};
