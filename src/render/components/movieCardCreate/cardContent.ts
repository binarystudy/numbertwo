import { createElement } from '../../helpers';

const overview = (overview: string): HTMLElement => {
    return createElement({
        tagName: 'p',
        attributes: {
            class: 'card-text truncate',
        },
        child: overview,
    });
};

const date = (date: string): HTMLElement => {
    const small = createElement({
        tagName: 'small',
        attributes: {
            class: 'text-muted',
        },
        child: date,
    });
    return createElement({
        tagName: 'div',
        attributes: {
            class: 'd-flex justify-content-between align-items-center',
        },
        child: small,
    });
};

export const cardContent = (
    id: number,
    description: string,
    releaseDate: string
): HTMLElement => {
    return createElement({
        tagName: 'div',
        attributes: {
            class: 'card-body',
        },
        child: [overview(description), date(releaseDate)],
    });
};
