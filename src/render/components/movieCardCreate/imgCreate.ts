import { createElement } from '../../helpers';

export const imgCreate = (src: string, alt: string): HTMLElement => {
    const img = document.createElement('img');
    if (!src) {
        return createElement({ tagName: 'div' });
    }
    img.src = src ? src : '';
    img.alt = alt;
    return img;
};
