import { MovieIdElements, MovieDataset } from '../../../config';
import { IMovie } from '../../../types';
import { createElement } from '../../helpers';
import { cardContent } from './cardContent';
import { favoriteCreate } from './favoriteCreate';
import { imgCreate } from './imgCreate';

const classes: { [key: string]: string } = {
    favorite: 'col-12 p-2',
    movie: 'col-lg-3 col-md-4 col-12 p-2',
};

export const movieCardCreate = (data: IMovie, type: string): HTMLElement => {
    const img = imgCreate(data.src, data.title);
    const favorite = favoriteCreate(data.id, data.isFavorites);
    const content = cardContent(data.id, data.overview, data.releaseDate);
    const card = createElement({
        tagName: 'div',
        attributes: {
            class: 'card shadow-sm',
        },
        child: [img, favorite, content],
    });
    return createElement({
        tagName: 'div',
        attributes: {
            class: classes[type],
            id: MovieIdElements.MOVIE_CARD,
            [`data-${MovieDataset.MOVIE_ID}`]: data.id.toString(),
        },
        child: card,
    });
};
