import { MovieIdElements } from '../../../config';

const favoriteIcon = document.getElementById(
    MovieIdElements.FAVORITE
) as HTMLTemplateElement;
const favoriteIconEmpty = document.getElementById(
    MovieIdElements.FAVORITE_EMPTY
) as HTMLTemplateElement;

export const favoriteCreate = (id: number, isFavorite: boolean): Node => {
    const favorite = isFavorite
        ? favoriteIcon.content
        : favoriteIconEmpty.content;
    return favorite.cloneNode(true);
};
