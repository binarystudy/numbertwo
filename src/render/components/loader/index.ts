import { createElement } from '../../helpers';

export const loader = (): HTMLElement => {
    const span = createElement({
        tagName: 'span',
        attributes: {
            class: 'visually-hidden',
        },
        child: 'Loading...',
    });
    const div = createElement({
        tagName: 'div',
        attributes: {
            class: 'spinner-grow text-dark',
            style: 'width: 3rem; height: 3rem;',
            role: 'status',
        },
        child: span,
    });
    const wrapper = createElement({
        tagName: 'div',
        attributes: {
            class: 'd-flex justify-content-center',
        },
        child: div,
    });
    return wrapper;
};
