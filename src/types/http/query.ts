export type HttpQuery = {
    [key: string]: string | number | (string | number)[];
};
