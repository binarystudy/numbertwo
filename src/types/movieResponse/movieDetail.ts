import { IMovieResponse } from './movie';

export interface IMovieDetailResponse extends IMovieResponse {
    budget: number;
    homepage: string | null;
    imdb_id: string | null;
    production_companies: {
        name: string;
        id: number;
        logo_path: string | null;
        origin_country: string;
    }[];
    production_countries: {
        iso_3166_1: string;
        name: string;
    }[];
    revenue: number;
    runtime: number | null;
    status: string;
    vote_count: number;
}
