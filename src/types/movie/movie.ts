export interface IMovie {
    src: string;
    overview: string;
    releaseDate: string;
    id: number;
    title: string;
    isFavorites: boolean;
}
