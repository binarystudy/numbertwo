import { IMovie } from '../movie/movie';

export interface IMoviesStore {
    reset: () => void;
    add: (newMovies: IMovie[]) => void;
    remove: (oldMovies: IMovie) => void;
    movies: () => IMovie[] | [];
    change: (movie: IMovie) => void;
    getElement: (id: number) => IMovie | null;
}
