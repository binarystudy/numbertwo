export interface ICreateElement {
    tagName: string;
    attributes?: { [key: string]: string };
    child?: HTMLElement | string | Node | (HTMLElement | string | Node)[];
}
