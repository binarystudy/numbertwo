export const BASE_URL = 'https://api.themoviedb.org/3';
export const IMAGE_URL = 'http://image.tmdb.org/t/p/w500';
export enum MOVIE_URL {
    PATH = 'movie',
    SEARCH = 'search/movie',
}
