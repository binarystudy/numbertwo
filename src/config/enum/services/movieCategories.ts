export enum MovieCategories {
    POPULAR = 'popular',
    TOP_RATED = 'top_rated',
    UPCOMING = 'upcoming',
    LOAD_MORE = 'load-more',
    SUBMIT = 'submit',
}
