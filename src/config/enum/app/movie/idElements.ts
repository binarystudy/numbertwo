export enum MovieIdElements {
    FAVORITE = 'favorite-icon',
    FAVORITE_EMPTY = 'favorite-icon-empty',
    FAVORITE_MOVIES = 'favorite-movies',
    FILM_CONTAINER = 'film-container',
    MOVIE_CARD = 'card-movie',
    SEARCH = 'search',
}
