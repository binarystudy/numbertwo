import { IMovie, IMoviesStore } from '../../types';

const MoviesStore = () => {
    let movies: IMovie[] = [];

    const reset = (): void => {
        movies = [];
    };

    const add = (newMovies: IMovie[]): void => {
        movies = [...movies, ...newMovies];
    };

    const remove = (oldMovie: IMovie): void => {
        movies = movies.filter((movie) => movie.id === oldMovie.id);
    };

    const change = (newMovie: IMovie): void => {
        movies = movies.map((movie) => {
            if (movie.id === newMovie.id) {
                return newMovie;
            }
            return movie;
        });
    };

    const getElement = (id: number): IMovie | null => {
        const movie = movies.filter((movie) => movie.id === id);
        if (!movie.length) {
            return null;
        }
        return movie[0];
    };

    return {
        reset,
        add,
        remove,
        movies: () => movies,
        change,
        getElement,
    };
};

export const moviesStore = MoviesStore() as IMoviesStore;
