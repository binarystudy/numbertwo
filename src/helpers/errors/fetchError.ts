import { AppError } from './appError';

export class FetchError extends AppError {
  status: number;
  constructor(message: string) {
    super(message);
    this.status = 404;
  }
}
