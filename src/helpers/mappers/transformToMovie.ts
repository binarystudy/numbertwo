import { IMAGE_URL } from '../../config';
import {
    IMovie,
    IMovieDetailResponse,
    IMovieTotalResponse,
    IMovieResponse,
} from '../../types';

export const transformToMovie = <
    T extends IMovieDetailResponse | IMovieResponse
>(
    data: T,
    favorites: number[] | null
): IMovie => {
    return {
        src: data.poster_path ? `${IMAGE_URL}${data.poster_path}` : '',
        overview: data.overview,
        releaseDate: data.release_date,
        id: data.id,
        title: data.title,
        isFavorites: favorites ? favorites.includes(data.id) : false,
    };
};

export const transformFromTotalToMovie = (
    data: IMovieTotalResponse,
    favorites: number[] | null
): IMovie[] => {
    return data.results.map((result) => transformToMovie(result, favorites));
};

export const transformFromDetailToMovie = (
    data: IMovieDetailResponse[],
    favorites: number[] | null
): IMovie[] => {
    return data.map((movie) => transformToMovie(movie, favorites));
};
