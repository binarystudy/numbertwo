import { stringify } from 'query-string';
import { HttpQuery } from '../../../types';

export const queryToString = (query: HttpQuery): string =>
    stringify(query, { arrayFormat: 'comma' });
