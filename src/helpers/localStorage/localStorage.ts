type Value = string | number | Array<number> | { [key: string]: string };

export class LocalStorage {
    private name: string;
    constructor(name: string) {
        this.name = name;
    }

    setItem(value: Value): void {
        const jsonValue = JSON.stringify(value);
        localStorage.setItem(this.name, jsonValue);
    }

    getItem(): unknown {
        const jsonValue = localStorage.getItem(this.name);
        try {
            return jsonValue ? JSON.parse(jsonValue) : null;
        } catch (e) {
            return null;
        }
    }
}
