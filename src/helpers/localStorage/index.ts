import { LocalStorage } from './localStorage';

export class FavoritesLocalStorage extends LocalStorage {
    constructor() {
        super('favorites');
    }
    getItem(): number[] | null {
        const value = super.getItem();
        if (Array.isArray(value)) {
            return value;
        }
        return null;
    }
}

export const favoritesLocalStorage = new FavoritesLocalStorage();
