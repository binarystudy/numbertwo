import { Http } from '../http';
import { API_KEY, BASE_URL } from '../../config';
import { HttpQuery } from '../../types';

abstract class ServicesRoot {
    http: InstanceType<typeof Http>;
    locale?: string;
    constructor(http: InstanceType<typeof Http>) {
        this.http = http;
    }

    getApiKey(): HttpQuery {
        return { api_key: API_KEY };
    }

    getQuery(query: HttpQuery | Record<string, never> = {}): HttpQuery {
        return {
            ...this.getApiKey(),
            ...query,
        };
    }

    getUrl(path?: string): string {
        return `${BASE_URL}${path ? `/${path}` : ''}`;
    }
}

export { ServicesRoot };
