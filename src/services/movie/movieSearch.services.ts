import { MOVIE_URL } from '../../config';
import { IMovieTotalResponse } from '../../types';
import { ServicesRoot } from '../abstract';

export class MovieSearch extends ServicesRoot {
    async load(query: string, page?: number): Promise<IMovieTotalResponse> {
        return (await this.http.load(this.getUrl(MOVIE_URL.SEARCH), {
            query: this.getQuery({ query, ...(page ? { page } : {}) }),
        })) as Promise<IMovieTotalResponse>;
    }
}
