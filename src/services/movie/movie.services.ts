import { MovieCategories, MOVIE_URL } from '../../config';
import { IMovieTotalResponse } from '../../types';
import { ServicesRoot } from '../abstract';

export class Movie extends ServicesRoot {
    async load(
        category: MovieCategories,
        page?: number
    ): Promise<IMovieTotalResponse> {
        return (await this.http.load(
            this.getUrl(`${MOVIE_URL.PATH}/${category}`),
            {
                query: this.getQuery(page ? { page } : {}),
            }
        )) as Promise<IMovieTotalResponse>;
    }
}
