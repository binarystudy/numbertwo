import { MOVIE_URL } from '../../config';
import { IMovieDetailResponse } from '../../types';
import { ServicesRoot } from '../abstract';

export class MovieDetail extends ServicesRoot {
    async load(id: number): Promise<IMovieDetailResponse> {
        return (await this.http.load(this.getUrl(`${MOVIE_URL.PATH}/${id}`), {
            query: this.getQuery(),
        })) as Promise<IMovieDetailResponse>;
    }
}
