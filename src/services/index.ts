import { Http } from './http';
import { Movie, MovieSearch, MovieDetail } from './movie';

const http = new Http();

const movieServices = new Movie(http);

const movieDetailServices = new MovieDetail(http);

const movieSearchServices = new MovieSearch(http);

export { movieServices, movieDetailServices, movieSearchServices };
