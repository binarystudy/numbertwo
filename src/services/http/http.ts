import { HttpMethods } from '../../config';
import { FetchError, queryToString } from '../../helpers';
import { HttpQuery } from '../../types';

class Http {
    async load(
        url: string,
        options: { method?: string; query?: HttpQuery } = {}
    ): Promise<unknown> {
        const { method = HttpMethods.GET, query } = options;
        try {
            const response = await fetch(this.getUrl(url, query), {
                method,
            });
            const response_1 = await this.checkStatus(response);
            return this.getJson(response_1);
        } catch (err) {
            return this.error(err);
        }
    }

    private getUrl(url: string, query?: HttpQuery): string {
        return `${url}?${query ? queryToString(query) : ''}`;
    }

    private async checkStatus(response: Response) {
        if (response.ok) {
            return response;
        }
        let textBody: string;
        try {
            textBody = await response.text();
            const parsedException = JSON.parse(textBody);
            const status = parsedException.status_code;
            if (
                (response.status === 404 || response.status === 410) &&
                (status === 7 || status === 34)
            ) {
                throw new FetchError('data could not be found.');
            }
            throw new Error(parsedException?.message ?? response.statusText);
        } catch (e) {
            if (e instanceof SyntaxError) {
                throw new FetchError('invalid data');
            } else {
                throw e;
            }
        }
    }

    private getJson(response: Response) {
        return response.json();
    }

    private error<Error>(err: Error) {
        throw err;
    }
}

export { Http };
